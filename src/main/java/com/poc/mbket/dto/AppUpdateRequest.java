package com.poc.mbket.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class AppUpdateRequest {
    @NotEmpty(message = "text required")
    private String text;
    @NotNull(message = "flag required")
    private Boolean flag;
}
