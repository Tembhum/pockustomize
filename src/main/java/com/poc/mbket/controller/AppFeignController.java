package com.poc.mbket.controller;

import com.poc.mbket.dto.AppRequest;
import com.poc.mbket.dto.AppResponse;
import com.poc.mbket.feign.OtherAppClient;
import com.poc.mbket.feign.SettradeClient;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Setter(onMethod_ = @Autowired)
@Slf4j
public class AppFeignController {
    private OtherAppClient otherAppClient;
    private SettradeClient settradeClient;

    @GetMapping(value = "/feign/settrade-document", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> settradeOpenApiDocument() {
        log.info("Call settrade home via feign client");
        return ResponseEntity.ok(settradeClient.document());
    }

    @PostMapping(value = "/feign/other-app/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> otherAppCreate(@RequestHeader("request_id") String requestId,
                                               @RequestBody @Valid AppRequest request) {
        log.info("Call other app create");
        otherAppClient.otherAppCreate(requestId, request);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/feign/other-app/retrieve/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AppResponse> otherAppCreate(@RequestHeader("request_id") String requestId, @PathVariable("id") Long id) {
        log.info("Call other app retrieve");
        return ResponseEntity.ok(otherAppClient.otherAppRetrieve(requestId, id));
    }
}
