package com.poc.mbket.config;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = "com.poc.mbket.feign")
public class FeignConfig {
}
