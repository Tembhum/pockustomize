def REPOSITORY = 'https://10.251.20.181:9999/narutep/tanzu-poc.git'
def IMAGE_NAME = '10.252.246.60:5000/tanzu-poc'
def TAG_VERSION // Will assign in build step
properties([
        buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '7', numToKeepStr: '5'))
])

pipeline {
    agent { label 'maven-builder' }

    tools {
        git 'Default'
        maven 'maven3'
    }
    stages {
        stage('Fetch source') {
            steps {
                script {
                    checkout scm
                }
            }
        }
        stage('Build and Test') {
            steps {
                script {
                    sh "mvn clean install"
                    def pom_version = sh script: 'mvn help:evaluate -Dexpression=project.version -q -DforceStdout', returnStdout: true
                    TAG_VERSION = "${pom_version}-${BUILD_NUMBER}"
                }
            }
        }
        stage('SonarQube analysis') {
            steps {
                withSonarQubeEnv(installationName: 'Sonarqube') { // You can override the credential to be used
                  sh 'mvn clean package sonar:sonar' 
                }
            }
        }
        stage("Quality Gate") {
            steps {
              timeout(time: 3, unit: 'MINUTES') {
                waitForQualityGate abortPipeline: true
              }
            }
        }
        stage('Build and tag Image') {
            steps {
                script {
                    sh "docker build -t ${IMAGE_NAME}:${TAG_VERSION} ."
                }
            }
        }
        stage('Publish image') {
            steps {
                script {
                    sh "docker push ${IMAGE_NAME}:${TAG_VERSION}"
                }
            }
        }
        stage('Git tag') {
            steps {
                script {
                    withCredentials([string(credentialsId: 'scm_git_token', variable: 'GIT_TOKEN')]) {
                        sh "git tag ${TAG_VERSION}"
                        sh "git push https://oauth2:${GIT_TOKEN}@${REPOSITORY} --tags"
                    }
                }
            }
        }
    }
    post {
        always {
            script {
                sh "docker rmi ${IMAGE_NAME}:${TAG_VERSION}"
            }
            deleteDir()
        }
    }
}
